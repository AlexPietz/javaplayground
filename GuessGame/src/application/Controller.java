package application;

import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;


public class Controller {
	
    @FXML
    private TextField numberGuessed;
    @FXML
    private Label status;
    @FXML
    private Label status2;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Button guessButton;
    @FXML
    private Button higherB;
    @FXML
    private Button lowerB;
    @FXML
    private Button correctB;
    

    /* START Game 1 Variables */
	int counter1 = 1;
	Random generator = new Random();
	int i = generator.nextInt(100);
    /* END Game 1 Variables */
    /* START Game 2 Variables */
	int counter2 = 1;
	int rangeMin = 0;
	int rangeMax = 100;
	int median = 50;
    /* END Game 2 Variables */
	
    @FXML
    public void initialize() {
    	label1.setText("The computer has randomly chosen an Integer between 0 and 100! \n Guess a number and the computer will tell you if the number it's \n chosen is smaller or bigger than the one you guessed!");
    }	

    public void doEvaluate(ActionEvent event) {
    	try {
    			int number = Integer.parseInt(numberGuessed.getText());
    		    if(number==i) 
    		    	status.setText("you guessed the number in " + counter1 +" steps!");
    		    else if (number > i)
    		    	status.setText("the number you are looking for is smaller!");
    		    else if (number < i)
    		    	status.setText("the number you are looking for is bigger!");
    		    counter1++;
    		}
    	catch (NumberFormatException x) {
    		status.setText("Error : Your number is not an Integer ("+ x.getMessage() +")");
    	}
    }
    
    public void newGame1(ActionEvent event) {
    	i = generator.nextInt(100);
    	status.setText("Your result will be shown here!");
    	counter1 = 1;
    };
    public void newGame2(ActionEvent event) {
    	label2.setText("Please think about an Integer between 0 and 100 and let the \n computer guess it by choosing  whether your number is higher \n or lower than the number shown");
    	counter2 = 1;
    	rangeMin = 0;
    	rangeMax = 100;
    	median = 50;
    	status2.setText("" + median);
    	higherB.setDisable(false);
    	lowerB.setDisable(false);
    };

    public void correct(ActionEvent event) {
    	label2.setText("I have correctly guessed your number in " + counter2 +" steps!");
    	higherB.setDisable(true);
    	lowerB.setDisable(true);
    };
    public void higher(ActionEvent event) {
    	rangeMin = median;
    	median = ((rangeMax-rangeMin) / 2)  +  rangeMin;
    	status2.setText("" + median);
    	counter2++;
    };
    public void lower(ActionEvent event) {
    	rangeMax = median;
    	median = ((rangeMax-rangeMin) / 2)  +  rangeMin;
    	status2.setText("" + median);
    	counter2++;
    };
}
